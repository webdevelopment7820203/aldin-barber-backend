﻿using AldinsWebApi.Model.Reservation;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AldinsWebApi.Repository.Interface;

public interface IReservationRepository
{
    public Task<List<CreateReservationResponseDTO>> Create(Reservation reservation);
    public Task<List<GetReservationResponseDTO>> GetByDateTime(DateTime dateTime);
    public Task<List<CancelReservationDTO>> Cancel(int id); 
    public Task<List<GetReservationResponseDTO>> GetAllReservations();


}
