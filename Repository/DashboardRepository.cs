﻿/*using AldinsWebApi.Model.Dashboard;
using AldinsWebApi.Repository.Interface;
using Dapper;
using LoginApi.Helper;

namespace AldinsWebApi.Repository;

public class DashboardRepository : IDashboardRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;

    public DashboardRepository(ISqlConnectionProvider sqlConnectionProvider)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
    }

    public async Task<Dashboard> GetDashboards()
    {
        const string getDashboardDataString = @"SELECT d.id,
                                                d.finished_reservations,
                                                d.open_reservations,
                                                d.date
                                            FROM dashboard d";


        using var connection = await _sqlConnectionProvider.GetConnection();


        var data = await connection.QueryFirstOrDefaultAsync<Dashboard>(getDashboardDataString);

        return data;
    }

    public Task UpdateDashboardData()
    {
        throw new NotImplementedException();
    }
}

*/