using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.CreatePersonDTO;
using AldinsWebApi.Model.Person.Person;
using AldinsWebApi.Model.Person.PersonLoginResponseDTO;
using AldinsWebApi.Repository.Interface;
using Dapper;
using LoginApi.Helper;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Npgsql.Internal;
using System.Security.Cryptography;
using System.Text;

namespace AldinsWebApi.Repository;

public class LoginRepository : ILoginRepository
{

    private readonly ISqlConnectionProvider _sqlConnectionProvider;

    public LoginRepository(ISqlConnectionProvider sqlConnectionProvider)
    {

        _sqlConnectionProvider = sqlConnectionProvider;

    }

    public async Task<Person> Get(string Email)
    {
        const string getPersonString = @"SELECT p.id,
                                                p.firstname,
                                                p.lastname,
                                                p.email,
                                                p.password,
                                                p.telephone_number,
                                                p.creation_time
                                        FROM persons p WHERE p.email = :Email";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var person = await connection.QueryFirstOrDefaultAsync<Person>(getPersonString, new { Email }); 

        return person;
    }


    public async Task<bool> ExistPerson(string personId)
    {
        const string getPersonByIdString = @"SELECT COUNT(*) 
                                             FROM persons p
                                             WHERE p.id = :PersonId";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var count = await connection.QueryFirstAsync<int>(getPersonByIdString, new { PersonId = personId });


        if (count < 0)
        {
            return false;
        }

        return true;

    }

    public async Task<PersonLoginResponseDTO> Create(CreatePersonDTO personDTO)
    {
        const string createPersonString = @"INSERT INTO
                                                persons (id,firstname,lastname,email,password,telephone_number,creation_time)
                                                VALUES(:PersonId,:Firstname,:Lastname,:Email,:Password,:TelephoneNumber,:CreationTime)";


        const string getCreatedPersonString = @"SELECT p.id,
                                                       p.firstname,
                                                       p.lastname,
                                                       p.email,
                                                       p.password,
                                                       p.telephone_number,
                                                       p.creation_time
                                                FROM persons p 
                                                WHERE p.id = :PersonId";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var PersonId = await GenerateUserId(); 
         
        await connection.ExecuteAsync(createPersonString, new {PersonId, personDTO.Email, personDTO.Password,  personDTO.CreationTime });

        var hashedPassword = SHA256ToString(personDTO.Password);

        await connection.ExecuteAsync(createPersonString, new { PersonId, personDTO.Firstname, personDTO.Lastname, personDTO.Email, Password = hashedPassword,personDTO.TelephoneNumber, personDTO.CreationTime });


        var getCreatePerson = await connection.QueryFirstOrDefaultAsync<PersonLoginResponseDTO>(getCreatedPersonString, new { PersonId = PersonId });
        return getCreatePerson; 

    }

    public async Task<bool> Validation(string email, string password)
    {
        const string validationString = @"SELECT COUNT(*)
                                      FROM persons p 
                                      WHERE p.email = @Email AND p.password = @Password";

        try
        {
            using var connection = await _sqlConnectionProvider.GetConnection();

            var hashedPassword = SHA256ToString(password);

            var count = await connection.QueryFirstOrDefaultAsync<int>(validationString, new { Email = email, Password = hashedPassword });


            return count > 0;
        }
        catch (Exception ex)
        {

            throw new ApplicationException("An error occurred during validation.", ex);
        }
    }

    public async Task<Person> Patch(PatchPersonRequestDTO person)
    {
        const string patchPersonString = @"UPDATE persons
                                           SET
                                           lastname = CASE WHEN :Lastname IS NOT NULL AND :Lastname <> '' THEN :Lastname ELSE lastname END,
                                           firstname = CASE WHEN :Firstname IS NOT NULL AND :Firstname <> '' THEN :Firstname ELSE firstname END,
                                           email = CASE WHEN :Email IS NOT NULL AND :Email <> '' THEN :Email ELSE email END,
                                           telephone_number = CASE WHEN :TelephoneNumber IS NOT NULL AND :TelephoneNumber <> '' THEN :TelephoneNumber ELSE telephone_number END,
                                           password = CASE WHEN :Password IS NOT NULL AND :Password <> '' THEN :Password ELSE password END,
                                           is_login = CASE WHEN :IsLogin IS NOT NULL THEN :IsLogin::boolean ELSE is_login END
                                           WHERE id = :PersonId";


        const string selectPatchedPerson = @"SELECT p.id,
                                                    p.firstname,
                                                    p.lastname,
                                                    p.email,
                                                    p.password,
                                                    p.telephone_number,
                                                    p.is_login,
                                                    p.creation_time
                                              FROM persons p WHERE p.id = :PersonId";


        using var connection = await _sqlConnectionProvider.GetConnection();


        var hasehdPassword = person.Password != null ? SHA256ToString(person.Password) : null;

        await connection.ExecuteAsync(patchPersonString, new { person.Lastname, person.Firstname, person.Email, person.TelephoneNumber, Password = hasehdPassword, person.IsLogin, PersonId = person.Id });

        var patchedPerson = await connection.QueryFirstOrDefaultAsync<Person>(selectPatchedPerson, new { PersonId = person.Id });


        return patchedPerson;

    }


    private async Task<string> GenerateUserId()
    {
            Random random = new Random();

        while (true)
        {
            var generateId = random.Next(1000, 9999).ToString();

            var existingPerson = await ExistPerson(generateId);

            if (existingPerson == true)
            {
                return generateId;

            }

        }

    }

    public static string SHA256ToString(string s)
    {
        using (var alg = SHA256.Create())
            return alg.ComputeHash(Encoding.UTF8.GetBytes(s)).Aggregate(new StringBuilder(), (sb, x) => sb.Append(x.ToString("x2"))).ToString();
    }

}