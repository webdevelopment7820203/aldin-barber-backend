﻿using AldinsWebApi.Model;
using AldinsWebApi.Model.Comment;
using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.Person;
using AldinsWebApi.Model.Reservation;
using AldinsWebApi.Repository.Interface;
using Dapper;
using LoginApi.Helper;
using Microsoft.EntityFrameworkCore;

namespace AldinsWebApi.Repository;

public class ReservationRepository : IReservationRepository
{

    private readonly ISqlConnectionProvider _sqlConnectionProvider;


    public ReservationRepository(ISqlConnectionProvider sqlConnectionProvider)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
    }


    public async Task<List<CreateReservationResponseDTO>> Create(Reservation reservation)
    {
        const string createReservationString = @"INSERT INTO appointment(person_id,
                                                                         date_time,
                                                                         creation_time)
                                                        VALUES(:PersonId,
                                                               :DateTime,
                                                               :CreationTime)";

        const string getReservationResponse = @"SELECT p.id,
                                                       p.firstname,
                                                       p.lastname,
                                                       a.date_time,
                                                       a.creation_time
                                                   FROM persons p 
                                                   INNER JOIN appointment a 
                                                   ON a.person_id = p.id 
                                                   WHERE a.person_id = :PersonId";
        ;


        using var connection = await _sqlConnectionProvider.GetConnection();


        await connection.QueryAsync(createReservationString, new { reservation.PersonId, reservation.DateTime, CreationTime = DateTime.Now });

        var reservations = new Dictionary<string, CreateReservationResponseDTO>();

        await connection.QueryAsync<Person, Reservation, List<CreateReservationResponseDTO>>(getReservationResponse, (person, reservation) =>
       {
           if (!reservations.TryGetValue(person.Id, out var reservationEntry))
           {
               reservationEntry = new CreateReservationResponseDTO()
               {
                   Firstname = person.Firstname,
                   Lastname = person.Lastname,
                   DateTime = reservation.DateTime

               };
               reservations.TryAdd(person.Id, reservationEntry);
           }

           return null;
       }, splitOn: "date_time", param: new { reservation.PersonId });

        return reservations.Values.ToList();

    }


    public async Task<List<GetReservationResponseDTO>> GetByDateTime(DateTime dateTime)
    {

        const string getReservationString =
            """
            SELECT p.id,
                                                               p.firstname,
                                                               p.lastname,
                                                               a.id,
                                                               a.date_time,
                                                               a.cancellation_time,
                                                               c.comment
                                                               FROM appointment a
                                                               INNER JOIN persons p on a.person_id = p.id
                                                               RIGHT JOIN comment c on p.id = c.person_id
            WHERE  a.date_time = :DateTime
            """;

        using var connection = await _sqlConnectionProvider.GetConnection();

        var reservations = new Dictionary<DateTime, GetReservationResponseDTO>();



        await connection.QueryAsync<Person, Reservation, CommentItem, List<GetReservationResponseDTO>>(
       getReservationString,
       (per, res, com) =>
       {
           if (!reservations.TryGetValue(res.DateTime, out var reservationEntry))
           {

               reservationEntry = new GetReservationResponseDTO()
               {
                   Id = res.Id,
                   DateTime = res.DateTime,
                   Firstname = per.Firstname,
                   Lastname = per.Lastname,
                   Comment = com.Comment
               };

               reservations.TryAdd(reservationEntry.DateTime, reservationEntry);
           }

           return null;
       },
       splitOn: "id,comment", param: new { DateTime = dateTime }

   );

        return reservations.Values.ToList();
    }

    public async Task<List<CancelReservationDTO>> Cancel(int id)
    {

        const string cancelReservationString = @"UPDATE appointment
                                                 SET is_cancelled = :IsCancelled,
                                                 cancellation_time = :CancellationTime
                                                 WHERE id = :Id";


        const string getCanceledReservation = @"SELECT a.id AS ID,
                                                       a.date_time,
                                                       p.id,
                                                       p.firstname,
                                                       p.lastname
                                                       FROM appointment a 
                                                       INNER JOIN persons p on a.person_id = p.id
                                                       WHERE a.id = :Id 
                                                       AND a.is_cancelled = true";

        using var connection = await _sqlConnectionProvider.GetConnection();

        await connection.QueryAsync(cancelReservationString, new { IsCancelled = true, CancellationTime = DateTime.Now, Id = id });

        var cancelledReseravtions = new Dictionary<int, CancelReservationDTO>();


        await connection.QueryAsync<Reservation, Person, CancelReservationDTO>(getCanceledReservation, (res, pers) =>
         {

             if (!cancelledReseravtions.TryGetValue(res.Id, out var reservationEntry))
             {
                 reservationEntry = new CancelReservationDTO()
                 {
                     Id = res.Id,
                     DateTime = res.DateTime,
                     Firstname = pers.Firstname,
                     Lastname = pers.Lastname,
                     PersonId = pers.Id,
                     CancellationTime = DateTime.Now
                 };

                 cancelledReseravtions.Add(res.Id, reservationEntry);
             }

             return reservationEntry;

         }, splitOn: "id", param: new { Id = id });


        return cancelledReseravtions.Values.ToList();

    }


    public async Task<List<GetReservationResponseDTO>> GetAllReservations()
    {
        const string forgivenReservationString = @"SELECT p.id,
                                                   p.firstname,
                                                   p.lastname,
                                                   a.id,
                                                   a.date_time,
                                                   a.cancellation_time,
                                                   c.comment
                                                   FROM appointment a
                                                   INNER JOIN persons p on a.person_id = p.id
                                                   RIGHT JOIN comment c on p.id = c.person_id";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var reservations = new Dictionary<DateTime, GetReservationResponseDTO>();

        await connection.QueryAsync<Person, Reservation, CommentItem, List<GetReservationResponseDTO>>(forgivenReservationString, (pers, res, com) =>
        {
            if (!reservations.TryGetValue(res.DateTime, out var reservationEntry))
            {
                reservationEntry = new GetReservationResponseDTO()
                {
                    Id = res.Id,
                    Firstname = pers.Firstname,
                    Lastname = pers.Lastname,
                    DateTime = res.DateTime,
                    Comment = com.Comment,

                };

                reservations.TryAdd(res.DateTime, reservationEntry);
            }

            return null;

        }, splitOn: "id,comment");

        return reservations.Values.ToList();

    }
}

