﻿using System.Data.Common;
using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.Person;
using Microsoft.EntityFrameworkCore;

namespace LoginApi.Model;

public class LoginContext : DbContext
{
    public LoginContext(DbContextOptions<LoginContext> options)
        : base(options)
    {
    }

    public DbSet<Person> LoginItems { get; set; } = null!;
}