﻿namespace AldinsWebApi.Model.Reservation;

public class CancelReservationDTO
{
    public int Id { get; set; }
    public string? PersonId { get; set; }
    public string? Firstname { get; set; }
    public string? Lastname { get; set; }
    public DateTime DateTime { get; set; }
    public DateTime? CancellationTime { get; set; }
}

