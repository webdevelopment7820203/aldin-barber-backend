﻿using Microsoft.Extensions.Primitives;

namespace AldinsWebApi.Model.Reservation
{
    public class CreateReservationResponseDTO
    {
        public int? Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DateTime { get; set; }
    }
}
