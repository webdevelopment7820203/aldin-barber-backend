﻿namespace AldinsWebApi.Model.Reservation;

    public class Reservation
    {
       public int Id { get; set; }  
       public DateTime DateTime { get; set; }
       public string PersonId { get; set; } 
    }

