﻿namespace AldinsWebApi.Model.Reservation;

public class GetReservationResponseDTO
{
    public int Id { get; set; }
    public DateTime DateTime { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string Comment { get; set; }
}