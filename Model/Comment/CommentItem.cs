﻿namespace AldinsWebApi.Model.Comment;
public class CommentItem
{
    public int Id { get; set; }
    public string Comment { get; set; }
    public string PersonId { get; set; }
}

