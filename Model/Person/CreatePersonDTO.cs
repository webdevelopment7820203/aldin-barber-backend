﻿namespace AldinsWebApi.Model.Person.CreatePersonDTO;

public class CreatePersonDTO
{
    public string Id { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string Email { get; set; }
    public string TelephoneNumber { get; set; }
    public string Password { get; set; }
    public DateTime CreationTime { get; set; }
}

