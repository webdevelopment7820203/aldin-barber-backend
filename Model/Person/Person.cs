﻿namespace AldinsWebApi.Model.Person.Person; 

    public class Person
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string Password { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsLogin { get; set; }   
    }
