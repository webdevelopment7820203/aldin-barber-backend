﻿namespace AldinsWebApi.Model.Person
{
    public class PatchPersonRequestDTO
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }    
        public bool IsLogin { get; set; }
    }
}
