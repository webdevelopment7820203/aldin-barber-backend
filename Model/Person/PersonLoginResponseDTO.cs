﻿namespace AldinsWebApi.Model.Person.PersonLoginResponseDTO;

public class PersonLoginResponseDTO
{
    public string Email { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public bool IsLoggedin { get; set; }
}

