using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.CreatePersonDTO;
using AldinsWebApi.Model.Person.Person;
using AldinsWebApi.Model.Person.PersonLoginResponseDTO;

namespace AldinsWebApi.Handler.Interface;

public interface ILoginHandler
{
    public Task<PersonLoginResponseDTO> Create(CreatePersonDTO personDTO);
    public Task<Person> Get(string email);
    public Task<bool> ExistPerson(string personId);
    public Task<bool> Validation(string email, string password);
    public Task<Person> Patch(PatchPersonRequestDTO person);

}

