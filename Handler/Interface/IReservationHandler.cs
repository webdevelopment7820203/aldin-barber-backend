﻿using AldinsWebApi.Model.Reservation;

namespace AldinsWebApi.Handler.Interface;

public interface IReservationHandler
{
    public Task<List<CreateReservationResponseDTO>> Create(Reservation reservation);
    public Task<List<GetReservationResponseDTO>> Get(DateTime dateTime);
    public Task<List<CancelReservationDTO>> Cancel(int id );
    public Task<List<GetReservationResponseDTO>> GetAllReservations();
}

