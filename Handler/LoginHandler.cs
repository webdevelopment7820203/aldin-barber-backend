﻿using AldinsWebApi.Handler.Interface;
using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.CreatePersonDTO;
using AldinsWebApi.Model.Person.Person;
using AldinsWebApi.Model.Person.PersonLoginResponseDTO;
using AldinsWebApi.Repository.Interface;

namespace AldinsWebApi.Handler;

public class LoginHandler : ILoginHandler
{

    private readonly ILoginRepository _loginRepository;

    public LoginHandler(ILoginRepository loginRepository)
    {
        _loginRepository = loginRepository;
    }

    public async Task<PersonLoginResponseDTO> Create(CreatePersonDTO personDTO)
    {
        return await _loginRepository.Create(personDTO);
    }

    public async Task<Person> Get(string Email)
    {
        return await _loginRepository.Get(Email);
    }

    public async Task<bool> ExistPerson(string personId)
    {
        return await _loginRepository.ExistPerson(personId);
    }

    public async Task<bool> Validation(string email, string password)
    {
        return await _loginRepository.Validation(email, password);
    }

    public async Task<Person> Patch(PatchPersonRequestDTO person)
    {
        return await _loginRepository.Patch(person);
    }
}

