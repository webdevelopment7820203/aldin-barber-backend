﻿using AldinsWebApi.Handler.Interface;
using AldinsWebApi.Model.Reservation;
using AldinsWebApi.Repository.Interface;

namespace AldinsWebApi.Handler;

public class ReservationHandler : IReservationHandler
{

    private readonly IReservationRepository _reservationRepository;

    public ReservationHandler(IReservationRepository reservationRepository)
    {
        _reservationRepository = reservationRepository;
    }

    public async Task<List<CreateReservationResponseDTO>> Create(Reservation reservation)
    {
        return await _reservationRepository.Create(reservation);
    }

    public async Task<List<GetReservationResponseDTO>> Get(DateTime dateTime)
    {
        return await _reservationRepository.GetByDateTime(dateTime);
    }

    public async Task<List<CancelReservationDTO>> Cancel(int id)
    {
        return await _reservationRepository.Cancel(id); 

    }

    public async Task<List<GetReservationResponseDTO>> GetAllReservations()
    {
        return await _reservationRepository.GetAllReservations(); 
    }
}

