﻿/*using AldinsWebApi.Handler.Interface;
using AldinsWebApi.Model.Dashboard;
using AldinsWebApi.Repository.Interface;
using Microsoft.AspNetCore.Mvc;

namespace AldinsWebApi.Controllers;


[ApiController]
public class DashboardController : ControllerBase
{
    private readonly IDashboardHandler _dashboardHandler;

    public DashboardController(IDashboardHandler dashboardHandler)
    {
        _dashboardHandler = dashboardHandler;
    }


    [Route("api/dashboard")]
    [HttpGet]
    public async Task<ActionResult<Dashboard>> GetDashboards()
    {
        var dashboardData = await _dashboardHandler.GetDashboardData();


        if (dashboardData == null)
        {
            return BadRequest("Es konnten keine Dashboard Daten gefunden werden!");
        }

        return Ok(dashboardData);
    }





}

*/