﻿using AldinsWebApi.Handler.Interface;
using AldinsWebApi.Model.Reservation;
using Microsoft.AspNetCore.Mvc;

namespace AldinsWebApi.Controllers;

[ApiController]
public class ReservationController : ControllerBase
{

    private readonly IReservationHandler _reservationHandler;

    public ReservationController(IReservationHandler reservationHandler)
    {
        _reservationHandler = reservationHandler;
    }


    [Route("api/reservation/{dateTime}")]
    [HttpGet]
    public async Task<ActionResult<List<GetReservationResponseDTO>>> Get(DateTime dateTime)
    {
        var reservations = await _reservationHandler.Get(dateTime);

        if (reservations.Count == 0)
        {
            return BadRequest("Es konnte kein Termin zu der Uhrzeit gefunden werden!");
        }

        return Ok(reservations);
    }


    [Route("api/reservation")]
    [HttpPatch]
    public async Task<ActionResult<List<CancelReservationDTO>>> Cancel(int id )
    {
        var res = await _reservationHandler.Cancel(id);

        if(res == null)
        {
            return BadRequest("Reservierung konnte nicht abgesagt werden, bitte versuchen Sie es erneut!"); 
        }

        return Ok(res);

    }

    [Route("api/reservation")]
    [HttpPost]
    public async Task<ActionResult<List<CreateReservationResponseDTO>>> Create(Reservation reservation)
    {

        if (reservation.PersonId == "")
        {
            return BadRequest("Du musst angemeldet sein um eine Reservierung durchzuführen"); 
        }

        var createdReservation = await _reservationHandler.Create(reservation);


        if (createdReservation.Count == 0)
        {
            return BadRequest("Termin konnte nicht reserviert werden, bitte versuche es erneut!");
        }

        return Ok(createdReservation);

    }

    [Route("api/reservation")]
    [HttpGet]
    public async Task<ActionResult<List<GetReservationResponseDTO>>> GetAllReservations()
    {
        var reservation = await _reservationHandler.GetAllReservations(); 

        if(reservation == null)
        {
            return BadRequest("Fehler beim lesen der Vergebenen Reservierungen, bitte versuche es später nochmal!"); 
        }

        return Ok(reservation); 

    }

}

