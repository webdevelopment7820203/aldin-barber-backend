

using AldinsWebApi.Handler.Interface;
using AldinsWebApi.Model;
using AldinsWebApi.Model.Person;
using AldinsWebApi.Model.Person.CreatePersonDTO;
using AldinsWebApi.Model.Person.Person;
using AldinsWebApi.Model.Person.PersonLoginResponseDTO;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AldinsWebApi.Controllers;


[ApiController]
public class Controller : ControllerBase
{

    private readonly ILoginHandler _loginHandler;

    public Controller(ILoginHandler loginHandler)
    {
        _loginHandler = loginHandler;
    }


    [Route("api/person")]
    [HttpGet]
    public async Task<ActionResult<PersonLoginResponseDTO>> Login(string email, string password)
    {
        var existPerson = await _loginHandler.Validation(email, password);

        if (!existPerson)
        {
            return BadRequest("Die E-Mail Adresse oder das Password ist falsch! Bitte geben Sie ihre Daten ein");
        }


        var person = await _loginHandler.Get(email);

        var patchedPerson = await _loginHandler.Patch(new PatchPersonRequestDTO()
        {
            Id = person.Id,
            IsLogin = true,
        });



        var logedInUser = new PersonLoginResponseDTO()
        {
            Email = patchedPerson.Email,
            Firstname = patchedPerson.Firstname ?? "",
            Lastname = patchedPerson.Lastname,
            IsLoggedin = patchedPerson.IsLogin,
        };

        return logedInUser;

    }

    [Route("api/person")]
    [HttpPost]
    public async Task<ActionResult<PersonLoginResponseDTO>> Create(CreatePersonDTO personDTO)
    {

        var existPerson = await _loginHandler.Get(personDTO.Email);


        if (existPerson != null)
        {
            return BadRequest(new {message = "Ein User mit dieser Email existiert bereits, Verwenden Sie bitte eine andere E-Mail Adresse oder Loggen Sie sich ein!", existPerson});
        }


        personDTO.CreationTime = DateTime.Now;

        var person = await _loginHandler.Create(personDTO);



        return Ok(person);
    }

    [Route("api/person")]
    [HttpPatch]
    public async Task<ActionResult<Person>> Patch(PatchPersonRequestDTO person)
    {
        var patchedPerson = await _loginHandler.Patch(person);

        if (patchedPerson == null)
        {
            return BadRequest("Ein Fehler ist aufgetreten, verusche es bitte erneut. Kontaktieren Sie uns sobald der Fehler erneut auftuacht!");
        }

        return Ok(patchedPerson);
    }

}


