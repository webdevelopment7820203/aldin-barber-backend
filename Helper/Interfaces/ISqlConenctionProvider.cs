﻿using System.Data;

namespace LoginApi.Helper;

public interface ISqlConnectionProvider
{
    Task<IDbConnection> GetConnection(string? connectionName = null);
}