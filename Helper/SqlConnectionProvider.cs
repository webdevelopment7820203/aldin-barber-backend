﻿using System.Data; 
using Npgsql;
namespace LoginApi.Helper;

public class SqlConnectionProvider : ISqlConnectionProvider
{
    private readonly IConfiguration _configuration;

    public SqlConnectionProvider(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<IDbConnection> GetConnection(string? connectionName = null)
    {
        if (string.IsNullOrEmpty(connectionName))
        {
            connectionName = "Default";
        }

        var connectionString = _configuration.GetConnectionString(connectionName);
        var con = new NpgsqlConnection(connectionString);
        await con.OpenAsync();
        return con;
    }
}